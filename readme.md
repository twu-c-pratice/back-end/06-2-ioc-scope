# 作业要求

## 1 Definition of Done

* 必须非常严格的按照题目中的定义进行实现。
* 每一种 case 都必须至少包含一个独立的测试。
* 所有的测试必须都成功。
* 注意代码的坏味道。尤其是命名的规范性以及重复代码的问题。

## 2 请书写测试来验证如下的 case

如果你已经忘记了之前的内容你可以观察 test/java/com/twuc/webApp/simplest 目录下的范例测试。它可以帮助你回忆起来。以下的所有测试请在 **test/java/com/twuc/webApp/yourTurn** 下书写。

### 2.1 Singleton 的使用

* 如果我有一个类型 `InterfaceOneImpl` 实现了接口 `InterfaceOne`，那么 `getBean(InterfaceOneImpl.class)` 和 `getBean(InterfaceOne.class)`。会不会得到同一个实例呢？写测试证明这一点。

* 类似上述问题。对于类型的继承呢？对基类和派生类类型调用 `getBean()` 方法会得到同一个实例么？写测试证明这一点。

* 仍然类似上述问题。如果假设类 `DerivedClass` 继承了类 `AbstractBaseClass`。且 `AbstractBaseClass` 是一个抽象类。那么分别对 `AbstractBaseClass.class` 和 `DerivedClass.class` 调用 `getBean()` 会出现什么现象呢？写测试验证这一点。

### 2.2 Prototype 的使用

* 第二种简单的 scope 就是 prototype scope 了。请将类型 `SimplePrototypeScopeClass` 设置为 prototype scope。并尝试使用 `getBean()` 方法获得两个实例的引用。那么这两个引用指向的是同一个对象呢还是不同的对象呢？请写测试证明这一点。

* 我们刚才看到了两种 scope。我相信你已经能够明白，我们可以通过 `getBean()` 的方法获得指定类型的实例。但是问题是，这个实例是什么时候创建的呢？请书写若干测试来证明（1）对于 singleton 的对象，类型的实例是什么时候创建的？（2）对于 prototype scope 的对象，类型的实例是什么时候创建的？

* Eager singleton initialization 有很多的好处。但是如果我无论如何也想在调用 `getBean()` 的时候再创建实例应该如何做呢？请书写测试来说明这种情况应该如何应对。

* 如果一个 prototype scope 的对象 `PrototypeScopeDependsOnSingleton`，依赖一个 singleton scope `SingletonDependent` 类型。那么请问如果多次调用 `getBean(PrototypeScopeDependsOnSingleton.class)` 的话（假设两次），那么请问 `PrototypeScopeDependsOnSingleton` 类型的实例创建了多少？而 `SingletonDependent` 类型的实例创建了多少？写测试证明你的猜想。

* 如果一个 singleton scope 的对象，`SingletonDependsOnPrototype` 依赖一个 prototype scope 的对象 `PrototypeDependent`。请问，如果多次调用 `getBean(SingletonDependsOnPrototype.class)` 的话（假设两次），那么 `SingletonDependsOnPrototype` 创建了多少实例？而 `PrototypeDependent` 创建了多少实例？写测试证明你的猜想。

### 2.3 Proxy Mode

* 现在有一个 singleton scope 的对象 `SingletonDependsOnPrototypeProxy` 依赖一个 `PrototypeDependentWithProxy`。如果我希望每一次调用 `getBean(SingletonDependsOnPrototypeProxy.class)` 且使用该实例中的 `PrototypeDependentWithProxy` 类型的 field 的方法时都会创建一个新的 `PrototypeDependentWithProxy` 实例。该如何进行配置？并写测试证明自己的解决方案是正确的。

* 如果 singleton scope 对象 `SingletonDependsOnPrototypeProxyBatchCall` 依赖 proxy mode 的 prototype scope 对象 `PrototypeDependentWithProxy`，并且singleton 中的一个方法连续调用了 prototype scope 类型的实例没有独立实现的方法（例如`toString`）两次，那么请问每一种对象实际创建了几个呢？写测试来证明你的想法。

* 现在有一个 singleton scope 的对象 `SingletonDependsOnPrototypeProxy` 依赖一个 `PrototypeDependentWithProxy`。你能够写测试证明你使用的 `PrototypeDependentWithProxy` 是一个 Proxy 么？