package com.twuc.webApp.yourTurn.prototype;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Scope("prototype")
public class SimplePrototypeScopeClass {
    private static List<String> logger = new ArrayList<>();

    public SimplePrototypeScopeClass() {
        logger.add("simplePrototypeScope was created!");
    }

    public void getNothing() {
        logger.add("simplePrototypeScope method has benn called!");
    }

    public static List<String> getLogger() {
        return logger;
    }

    public static int getLoggerSize() {
        return logger.size();
    }
}
