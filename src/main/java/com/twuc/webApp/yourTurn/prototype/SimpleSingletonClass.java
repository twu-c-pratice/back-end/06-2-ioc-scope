package com.twuc.webApp.yourTurn.prototype;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class SimpleSingletonClass {
    private static List<String> logger = new ArrayList<>();

    public SimpleSingletonClass() {
        logger.add("simpleSingleton was created!");
    }

    public static List<String> getLogger() {
        return logger;
    }

    public static void getNothing() {
        logger.add("simpleSingleton method has benn called!");
    }

    public static int getLoggerSize() {
        return logger.size();
    }
}
