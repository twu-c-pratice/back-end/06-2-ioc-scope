package com.twuc.webApp.yourTurn.prototype;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

public class PrototypeUsingTest {
    private static AnnotationConfigApplicationContext context;

    @BeforeAll
    static void initApplicationContext() {
        context = new AnnotationConfigApplicationContext("com.twuc.webApp.yourTurn.prototype");
    }

    @Test
    void should_get_different_beans() {
        SimplePrototypeScopeClass firstBean = context.getBean(SimplePrototypeScopeClass.class);
        SimplePrototypeScopeClass secondBean = context.getBean(SimplePrototypeScopeClass.class);

        assertNotSame(firstBean, secondBean);
    }

    @Test
    void should_know_when_instance_was_created() {
        assertEquals(0, SimplePrototypeScopeClass.getLoggerSize());
        assertEquals(1, SimpleSingletonClass.getLoggerSize());
        assertIterableEquals(
                Arrays.asList(new String[] {"simpleSingleton was created!"}),
                SimpleSingletonClass.getLogger());

        SimplePrototypeScopeClass simplePrototype =
                context.getBean(SimplePrototypeScopeClass.class);
        SimpleSingletonClass simpleSingleton = context.getBean(SimpleSingletonClass.class);

        assertEquals(1, simplePrototype.getLoggerSize());
        assertEquals(1, simpleSingleton.getLoggerSize());
    }
}
