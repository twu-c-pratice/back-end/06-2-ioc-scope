package com.twuc.webApp.yourTurn.singleton;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.junit.jupiter.api.Assertions.*;

public class SingletonUsingTest {
    private AnnotationConfigApplicationContext context;

    @BeforeEach
    void initApplicationContext() {
        context = new AnnotationConfigApplicationContext("com.twuc.webApp.yourTurn");
    }

    @Test
    void should_get_same_instance() {
        InterfaceOne interfaceOne = context.getBean(InterfaceOne.class);
        InterfaceOneImpl interfaceOneImpl = context.getBean(InterfaceOneImpl.class);

        assertSame(interfaceOne.getClass(), interfaceOneImpl.getClass());
    }

    @Test
    void should_get_throw_when_using_base_and_derive_class() {
        assertThrows(
                RuntimeException.class,
                () -> {
                    BaseClass baseClass = context.getBean(BaseClass.class);
                    DeriveClass deriveClass = context.getBean(DeriveClass.class);
                });
    }

    @Test
    void should_get_different_when_get_bean_using_abstract_and_derive_class() {
        AbstractBaseClass abstractBaseClass = context.getBean(AbstractBaseClass.class);
        DeriveClass deriveClass = context.getBean(DeriveClass.class);
        assertNotSame(abstractBaseClass.getClass(), deriveClass.getClass());
    }
}
